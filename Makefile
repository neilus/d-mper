all: rpm
VERSION = 1.0
RELEASE = 0
SOURCE_FILES =   d-mper-libs.sh  dumper.sh sentinel.sh
CONFIG_FILES = d-mper.conf  neilus.repo
SPECS_FILES  =  SPECS/d-mper.spec
RPM_DIRS = BUILD RPMS SRPMS BUILD SOURCES

clean:
	rm -rf ${RPM_DIRS}
install: ${SOURCE_FILES} /usr/local/libexec/nagios
	mkdir -p /etc/d-mper
	install -c --backup=existing -m 744 d-mper-libs.sh /etc/d-mper/
	install -c --backup=existing -m 744 sentinel.sh /usr/local/libexec/nagios/
	install -m 755 dumper.sh /usr/sbin
	install -c --backup=existing -m 755 neilus.repo /etc/yum.repos.d/

.git:
	git init
	git remote add origin https://bitbucket.org/neilus/d-mper.git
update:.git
	git pull
	@#install -c --backup=existing -m 744 d-mper.conf /etc/d-mper/
	@#make config
	make install
config:/root/.ssh/id_rsa_blank /etc/d-mper/d-mper.conf
	@tput setf 3 && echo "press any key to view the config file diff... " && tput sgr0
	@read
	vimdiff -n /etc/d-mper/d-mper.conf /etc/d-mper/d-mper.conf~
	@tput setf 3 && echo "press any key to edit the crontab... " && tput sgr0
	@read
	crontab -u root -e
/etc/d-mper/d-mper.conf:
	install -c --backup=existing -m 744 d-mper.conf /etc/d-mper/
/root/.ssh/id_rsa_blank:
	ssh-keygen -f $@ -q -N ''
/usr/local/libexec/nagios:
	mkdir -p $@
/etc/d-mper:
	mkdir -p $@
## SOURCES/d-mper-${VERSION}.tar.gz: ${SOURCE_FILES} ${RPM_DIRS}
## 	@## mkdir -p     ${notdir ${patsubst %.tar.gz,%'',$@}}
## 	@## cp  ${SOURCE_FILES} ${notdir ${patsubst %.tar.gz,%'',$@}}
## 	rpmbuild -ba ${SPECS_FILES}
## 	tar -cvzf $@ BUILD/${notdir ${patsubst %.tar.gz,%'',$@}}
##
##d-mper-${VERSION}.${RELEASE}.rpm: SOURCES/d-mper-${VERSION}.tar.gz ${SPECS_FILES}
##
##rpm: ${RPM_DIRS}
##
tarball: SOURCES/d-mper-${VERSION}.tar.gz
##
##BUILD:
##	mkdir -p $@
##RPMS:
##	mkdir -p $@
##SRPMS:
##	mkdir -p $@
##BUILD:
##	mkdir -p $@
##SOURCES:
##	mkdir -p $@
##
