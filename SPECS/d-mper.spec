Summary: MySQL dumper tool
Name: d-mper
Version: 1
Release: 0
License: GPL
Group: System Tools
URL: https://bitbucket.org/neilus/d-mper
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

Requires: coreutils, bash, mysql

%description
dömper is yet a nother MySQL dumper tool. The goal is to create a very simple backup, script collection, with tools which makes easy to configure full and incremental backups, 

%prep
%setup -q

%build

%install
mkdir -p /etc/d-mper
install -c --backup=simple -m 744 d-mper.conf d-mper-libs.sh -t /etc/d-mper/
install -c --backup=simple -m 744 sentinel.sh /usr/local/libexec/nagios/
install -m 755 dumper.sh /usr/sbin
install -c --backup=simple -m 755 neilus.repo /etc/yum.repos.d/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/etc/d-mper/d-mper.conf
/etc/d-mper/d-mper-libs.sh
/usr/local/libexec/nagios/sentinel.sh
/usr/sbin/dumper.sh
/etc/yum.repos.d/neilus.repo

%doc

%post
if [ -f /etc/d-mper/d-mper.conf~ ];then
	mv /etc/d-mper/d-mper.conf~ /etc/d-mper/d-mper.conf;
fi

%changelog
* Tue Apr 30 2013  <neilus@icss.hu> - d-mper-1.0
- Initial build.

