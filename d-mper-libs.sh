weekdays[0]="Sunday"
weekdays[1]="Monday"
weekdays[2]="Tuesday"
weekdays[3]="Wednesday"
weekdays[4]="Thursday"
weekdays[5]="Friday"
weekdays[6]="Saturday"
weekdays[7]="Sunday"

function dump_singlefile {
	export db_string="ALL";
	if [ $@ -ne "" ];then
		export db_string=`echo $@|sed 's/,/_/g'`
	fi
	echo "$DESTDIR/$DUMPFILE_PREFIX""_single_dump_""$db_string"_"$DUMPFILE_SUFFIX";
	return 0;
}
function dump_filename {
	export db_string="ALL";
	if [ $@ -ne "" ];then
		export db_string=`echo $@|sed 's/,/_/g'`
	fi
	echo "$DESTDIR/$DUMPFILE_PREFIX"`date +%u`_"$db_string"_"$DUMPFILE_SUFFIX";
	return 0;
}
function dump_long_filename {
	if [ $@ -ne "" ];then
		export db_string=`echo $@|sed 's/,/_/g'`
	else
		export db_string="ALL";
	fi
	echo "$DESTDIR/$DUMPFILE_PREFIX"`date +%Y-%m-%d`_${weekdays[`date +%u`]}_"$db_string"_"$DUMPFILE_SUFFIX";
	return 0;
}
function dump_howmanyToKeep { ### this is the new shit
	DUMPFILENAME=`dump_singlefile $@`;
	echo "$@" |/usr/bin/grep 'keep-weekly';
	if [ $? -eq 0 ];then
		params=`echo $@|sed 's/--keep-weekly//g'`;
		DUMPFILENAME=`dump_filename $params`;
	fi
	echo "$DUMPFILENAME";
}
function archive {
	# echo "$BW_LIMIT $SSH_KEY $1 $DR_SITE"
	/usr/bin/scp  -B -l $BW_LIMIT -i $SSH_KEY $1 $file $DR_SITE
	return "$?"
}

function db_params {
	db_string=`echo $@|sed 's/,/ /g'`;
	echo "$db_string";
}
