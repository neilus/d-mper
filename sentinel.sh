#!/bin/bash

################################################################################
# Sample Nagios plugin to monitor free memory on the local machine             #
# Author: Daniele Mazzocchio (http://www.kernel-panic.it/)                     #
################################################################################

VERSION="Version 0.1"
AUTHOR="(c) 2013 Norbert Sana (neilus@icss.hu)"

PROGNAME=`$0`

# Constants

# Exit codes
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Helper functions #############################################################

function print_revision {
   # Print the revision number
   echo "$PROGNAME - $VERSION"
}

function print_usage {
   # Print a short usage statement
   echo "Usage: $PROGNAME "
   echo "Whatewer parameters you give, it will simply ignore it"
}

function print_help {
   # Print detailed help information
   print_revision
   echo "$AUTHOR\n\nCheck free memory on local machine\n"
   print_usage

   /bin/cat <<__EOT

Options:
-h
   Print detailed help screen
-V
   Print version information

__EOT
}


# Verbosity level
verbosity=0
# Warning threshold
thresh_warn=
# Critical threshold
thresh_crit=

# Parse command line options

## this doesn't really do anything, but a nagios plugin might have these options
while [ "$#" -gt 0 ]; do
   case "$1" in
       -h | --help)
           print_help
           exit $STATE_OK
           ;;
       -V | --version)
           print_revision
           exit $STATE_OK
           ;;
       -v | --verbose)
           : $(( verbosity++ ))
           shift
           ;;
       -w | --warning | -c | --critical)
           shift 
           ;;
       -?)
           print_usage
           exit $STATE_OK
           ;;
       *)
           echo "$PROGNAME: Invalid option '$1'"
           print_usage
           exit $STATE_UNKNOWN
           ;;
   esac
done
if [ -f /var/log/d-mper.log ];then
	/bin/grep "Error" /var/log/d-mper.log >> /dev/null
	if [ "$?" -eq 0 ];then
		exit $STATE_CRITICAL;
	else
		/bin/grep "Warning" /var/log/d-mper.log >> /dev/null
		if [ "$?" -eq 0 ];then
			exit $STATE_WARNING;
		fi
		exit $STATE_OK;
	fi
fi

exit $STATE_UNKNOWN;

