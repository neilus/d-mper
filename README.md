for more detailed instructions visit the project's [wiki](https://bitbucket.org/neilus/d-mper/wiki/Home)

If you found a bug, please file me a [bug report](https://bitbucket.org/neilus/d-mper/issues?status=new&status=open)
d-mper is free software licensed under the terms of GPLv3

Installing d-mper
=================

installing from source tarball
------------------------------
Just grab it from bitbucket via git or the tarball, the master branch is considered as stable, might be called for production.
If you get into the sources root directory there you might call

```
#!bash
[ me@mysql ~ ]$ wget https://bitbucket.org/neilus/d-mper/get/master.tar.gz
[ me@mysql ~ ]$ tar xvzf master.tar.gz
[ me@mysql ~ ]$ cd *d-mper*
## you need to be root to install d-mper in the system directorys
[ me@mysql d-mper ]$ su
[ me@mysql d-mper ]$ make install
```

installing git on RHEL using the EPEL repository
------------------------------------------------
If you don't have git installed do so as described at http://git-scm.com/download/linux

If You run Red Hat Enterprise linux, you first should install the EPEL package repository.
You can read more about EPEL [on the Fedoraproject's WIKI - by RedHat ]( http://fedoraproject.org/wiki/EPEL).
You can install EPEL as described at [the fedoraproject wiki](http://fedoraproject.org/wiki/EPEL/FAQ#How_can_I_install_the_packages_from_the_EPEL_software_repository.3F).
```
#!bash
## You need root privileges to install the EPEL repository and GIT
[ me@mysql ~ ]$ su -
## Installing EPEL package repository
## On Red Hat Enterprise Linux 5.x
[ me@mysql ~ ]$  rpm -Uvh http://download.fedoraproject.org/pub/epel/5/i386/epel-release-5-4.noarch.rpm
## On Red Hat Enterprise Linux 6.x
[ me@mysql ~ ]$ rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
## You can install GIT by:
[ root@mysql ~ ]$ yum install git
```

installing from source using git
--------------------------------
```
#!bash
## after you have GIT installed you can clone the source code repository from
## bitbucket and installing it on youre system by issuing
[ me@mysql ~ ]$ git clone https://bitbucket.org/neilus/d-mper.git
[ me@mysql ~ ]$ cd d-mper
[ me@mysql d-mper ]$ su
[ root@mysql d-mper ]$ make install
```
The above listed command will escelate and will run with root privileges.
This will install it files to the proper places on the system.

Note that if you don't yet have a key for the root user, it will attempt to
create one, there you should leave the password blank, otherwise d-mper can not
run scheduled non-interactively as a cron job.

Also note that, in the end of the config you will be prompted to configure
d-mper. It will always copy the default config file, but you'll be prompted
with vimdiff and be able to see the last configuration of yours. So if there
would be modifications in the config file format, or new config options will be
available you should notice whenn issuing make config as root.

Update using git
----------------
If you have GIT installed, then you can issue the following command:
```
#!bash
[ root@mysql d-mper ]$ make update
```
If you do __not__ have GIT installed, for update you better download the newer
version of d-mper, and do it as described at the clean install section. It is
highly recomendded, to use GIT for getting d-mper. Only install from archive,
if you have no and may not install GIT on your system. Also please do not send
bugs if you haven't got it from GIT, becouse you might be confused of the
specific version you use, and without the changeset number I do not want to
waste any time for debuging.

Configuring d-mper
==================

You only have to issue the command:
```
#!bash
[ root@mysql d-mper ]$ make config
```
After issuing this command, you will be prompted for editing the d-mper config
file and the root users crontab configuration. 

d-mper config format
--------------------

config variable | description
 --- | ---
 USERNAME | The MySQL username which has rights to back up the database
 PASSWORD | your user's password which identifies it against MySQL
 LOG      | this is where d-mper logs (it is mandatory, if you'd like to use the sentinel module!)
 DESTDIR  | The destination directory, where d-mper backs up locally
 DUMPFILE_PREFIX | the prefix to the dump files it will create
 DUMPFILE_SUFFIX | the suffix to the dump files it will create
 BW_LIMIT | Bandwith limit in kb, this is given to scp when it copies to the DR-site
 DR_SITE | the dr-sites address
 SSH_KEY | the private ssh-key to use when authenticating with scp to the dr-site. this must *not* have a password.

scheduling backup jobs with crontab
------------------------------------
After you have the d-mper config, you can set up scheduled runs of d-mper from
youre systems cron daemon as described at the [RedHat Customer Portal](https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/Deployment_Guide/ch-autotasks.html).
d-mper should be started by the root user, so you should add the automated task to the root users crontab setting.

For configuring d-mper you need to be logged in with the root user, so you have
privileges for the system directories and files (like /etc and /var/log. You
only have to go trough this once, after you have installed d-mper already.

ssh-keys to copy the backups to the archive-/DR-repository trough scp
---------------------------------------------------------------------
Note that this is with the default configuration, you may use other ssh-keys,
or make them on your own, but the config command creates for you a blank
private/public key under /root/.ssh/id_rsa_blank[.pub].

```
#!bash
## firstly copy the public key to the remote site
[ root@mysql d-mper ]$ scp /root/.ssh/id_rsa_blank.pub user@archive.dr:
## then login to the remote machine
[ root@mysql d-mper ]$ ssh user@archive.dr
## set up the public key, enabling passwordless login to the mysql server with
## the key
[ user@archive.dr ~ ]$ cat id_rsa_blank.pub >> .ssh/authorized_keys
## check, if everything went as expected...
[ user@archive.dr ~ ]$ tail -n1 .ssh/authorized_keys|diff id_rsa_blank.pub -
## if the above command didn't prompted anything, then it should be OK
## now you might remove the temporary file you copied with scp
[ user@archive.dr ~ ]$ rm id_rsa_blank.pub
```

The first command will copy your public key to the archive computer at your
DR-site. Note  that it will prompt you for password, this is the same user and
that you use in the d-mper.conf file at the DR_SITE section (remember
DR_SITE='user@archive.dr:/archive/location/on/that/machine').
After this step you need to log in on that machine, and add this public key to
your authorized keys configuration, for that user (these are the other 2
commands).
If you have an ssh-key with a blank password, and completed the above steps,
d-mper (and you too) should be able to log in to the archive machine without
password. You should test this whit the following commands.

```
#!bash
[ root@mysql ~ ]$ ssh i ~/.ssh/id_rsa_blank user@archive.dr
[ user@archive.dr ~]$ 
```

Note that if you cannot login in passwordless, d-mper will not be able to do so
eighter, so it will log unsuccessfull backup, if it's unable to copy your
backup to the archive location. If you use the nagios plugin, then it will show
an error because of this too.

Running d-mper
==============

After you have made your config file, you can run the dumper script. The command dumper.sh should be in your root-path. The script might be run simply, without parameters (then it will try to back up all the mysql databases on the system), or you can give it the database names with comma separated, as paramaters.

```
#!bash
[ root@mysql ~ ]$ /usr/sbin/dumper.sh ## this will try to back up all the databases
[ root@mysql ~ ]$ /usr/sbin/dumper.sh drupal,test ## this will only back up the drupal and test databases
```
