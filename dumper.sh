#!/bin/bash

if [ -f /etc/d-mper/d-mper.conf ];then
	source /etc/d-mper/d-mper.conf
	source /etc/d-mper/d-mper-libs.sh
else
	source d-mper.conf
	source d-mper-libs.sh
fi
export dbs="$@"
export BACKUP=`dump_singlefile $@`
export LONG_FILENAME=`dump_long_filename $@`
mkdir -p $DESTDIR
rm -f $BACKUP >>/dev/null
touch $BACKUP
export retval=0
rm $LOG

function dump {
	if [ $# -eq 0 ];then ## double-check...
		export databases=" --all-databases ";
	else
		export databases=" --databases $@";
	fi
	script -aqc 'echo -e "MySQL dump starting on...\t$databases \tin the output $BACKUP"' $LOG
	mysqldump $databases --single-transaction --flush-logs --master-data=`date +%u` -u$USERNAME>$BACKUP -p$PASSWORD
	export retval=$?
	if [ "$retval" -eq 0 ]; then
		script -q -ac 'tput setf 2 && echo -n " OK " && tput sgr0 && echo ' $LOG
		logger  " [INFO:d-mper] mysqldump terminated succesfully";
		cp -l $BACKUP $LONG_FILENAME
		export retval=`expr $retval \| $?`
		if [ "$retval" -ne 0 ]; then
			script -qac 'tput setf 4 && echo -n "Error creating hard link Error code: $retval"&& tput sgr0 && echo' $LOG
			export retval=2;
		else
			script -qac 'echo -ne "Sending the dump to the DR-site...\t"' $LOG
			archive $LONG_FILENAME
			export retval=`expr $retval \| $?`
			#echo "$retval";
			if [ $retval == 0 ]; then
				script -qac 'tput setf 2 && echo -n "OK" && tput sgr0 &&echo' $LOG
				script -qac 'removing $BACKUP file' $LOG
				rm $BACKUP >>/dev/null
			else
				script -qac 'tput setf 6 && echo -n "Warning: sending dump to the DR-site! " && tput sgr0 && echo' $LOG
				export retval=3;
			fi
		fi
	#	tput setb 2 && tput setf 1 && echo  " mysqldump terminetad succesfully " >>script $LOG
	else
		script -q -ac 'tput setf 4 && echo -n " Error $retval has occured during backup " && tput sgr0 && echo' $LOG
		logger  " [ERROR:d-mper] mysqldump exited with status $retval ";
		export retval=1;
	#	tput setb 4 && tput setf 6 && echo  " Error $? has occured during backup " >>script $LOG
	fi
} ### end of the dump function
function delete_unnecessary { # Quick and kinda dirty implementation
  backups=$(ls -t $DESTDIR|grep -i $DUMPFILE_SUFFIX)
  MAXDUMPS=$(expr $MAXDUMPS \+ 1)
  unnecesarry=$(echo $backups|sed 's/\n/ /g'|cuf -d' ' -f$MAXDUMPS-)

  for i in $unnecesarry;
  do
    export i;
    script -qac 'tput setf 6 && echo -n "removing " && tput sgr0 && echo " $i ..."'
  done
}
### let's start the dump with the correct parameters
these=`db_params $@`
dump $these;
delete_unnecessary
grep -v 'Script started on ' $LOG > /tmp/d-mper.$$
cat /tmp/d-mper.$$ > $LOG
rm -f /tmp/d-mper.$$
#exit $retval
